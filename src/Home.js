import React, { Component } from 'react';
import { Table, TableHead, TableHeader, TableRow, TableBody, TableData } from 'carbon-components-react';
import Header from './Header';
import 'carbon-components/scss/globals/scss/styles.scss';
import './App.css';

class Home extends Component {
  render() {
    const tableItems = [
      { user: 'John McKeever', executed: '2018-Dec-18 11:45:44', message: 'Test commit', status: 'success' }
    ];

    const tableData = tableItems.map(({ user, executed, message, status}, index) => (
      <React.Fragment>
        <TableData className="App-table--data">{user}</TableData>
        <TableData className="App-table--data">{executed}</TableData>
        <TableData className="App-table--data">{message}</TableData>
        <TableData className="App-table--data">{status}</TableData>
      </React.Fragment>
    ));
  
    return (
      <div className="App">
        <div className="App-content">
          <Header />
          <main>
            <h1 className="App-heading">Commit History</h1>
            <Table>
              <TableHead>
                <TableRow header>
                  <TableHeader>User</TableHeader>
                  <TableHeader>Executed</TableHeader>
                  <TableHeader>Message</TableHeader>
                  <TableHeader>Status</TableHeader>
                </TableRow>
              </TableHead>
              <TableBody>
                {tableData}
              </TableBody>
            </Table>
          </main>
        </div>
      </div>    
    );
  }
}

export default Home;
