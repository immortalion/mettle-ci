import React from 'react'
import { Footer } from 'carbon-components-react';
import Sidebar from './Sidebar'
import Main from './Main'

const App = () => (
  <div>
    <Sidebar />
    <Main />
    <Footer>2018 Data Migrators</Footer>
  </div>
);

export default App;