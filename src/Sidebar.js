import React from 'react';
import { Link } from 'react-router-dom';
import { Icon } from 'carbon-components-react';
import { iconAdd } from 'carbon-icons';
import cx from 'classnames';
import 'carbon-components/scss/globals/scss/styles.scss';
import './App.css';

const menuItems = [
  { name: 'Unit Test', link: '/unit-test' },
  { name: 'Compliance', link: '/compliance' },
  { name: 'Version Control', link: '/version-control' },
];

const items = menuItems.map(({ name, link }, index) => (
  <div className="App-sidebar--item">
    <Icon
      icon={iconAdd}
      fill="white"
      description="This is a description of the icon and what it does…"
    />
    <Link
      to={link}
      key={index}
      className={cx({
        item: true,
        active: link === window.location.pathname,
      })}>
      {name}
    </Link>
  </div>
));

const Sidebar = () => (
  <aside className="App-sidebar">
      <div className="App-logo">
        <span className="App-brand">dm</span> MettleCI
      </div>
      <div className="App-nav">
        Navigation
      </div>
      {items}
    </aside>
)

export default Sidebar;