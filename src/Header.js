import React from 'react';
import { Icon } from 'carbon-components-react';
import { iconHeaderUser } from 'carbon-icons'
import 'carbon-components/scss/globals/scss/styles.scss';

const Header = () => (
  <header className="App-header">
    <div className="App-user">
      <Icon
        icon={iconHeaderUser}
        fill="gray"
        description=""
      />
      <span className="App-user--name">John McKeever</span>
    </div>
  </header>
);

export default Header;