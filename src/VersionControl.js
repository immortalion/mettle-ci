import React, { Component } from 'react';
import { TextInput, TextArea } from 'carbon-components-react';
import Header from './Header';
import 'carbon-components/scss/globals/scss/styles.scss';
import './VersionControl.css';

class VersionControl extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-content">
          <Header />
          <main>
          <TextInput
            className="VersionControl-input"
            id="test2"
            labelText="DataStage Project"
            type="readonly"
            placeholder="Placeholder text"
          />
          <TextArea 
            className="VersionControl-input"
            labelText="Commit Message"
            invalidText="A valid value is required"
            placeholder="Test commit"
          />
          </main>
        </div>
      </div>    
    );
  }
}

export default VersionControl;
